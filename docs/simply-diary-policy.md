
# Simply Diary Policy

> Welcome to Simply Diary! Writing diary is now more fun with background ambient sound. 

## Privacy Policy

    To bring you the best experience, we use social network credential to quickly register you to use the application
    Despite using Social Network to authenticate, we, until now, do not share or commercialize your personal information like name, email address, birthday, etc…
    This app also access to the internet, microphone, location and calendar events. All the information will be collected at your notice and your wish.
    We make reasonable efforts to protect your Personal information, but no company, including us, can fully eliminate security risks connected to handling information on the internet

## Privacy policy updates:
    This policy is subject to change without notice. Please check this page for the latest privacy policy. If you disagree with this policy, please discontinue use of the application.

## Contacting Us:
    If you have any questions or concerns regarding my privacy policies, please send me a detailed message to quoctrunguit@gmail.com

