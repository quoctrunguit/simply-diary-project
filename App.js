/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  Button,
  TouchableOpacity,
  Image,
  FlatList,
  Alert,
  PermissionsAndroid
} from 'react-native';

import firebase from 'react-native-firebase'

import { NavWrapper, Tabs } from './app/config/router';

import * as helpers from './app/helpers/helpers'

const FBSDK = require('react-native-fbsdk');

const {
  LoginButton,
  AccessToken
} = FBSDK;

const loggedIn = false;

export default class App extends Component<{}> {

  constructor(props)
  {
    super(props)
    this.state={
      login: false,
      authenticated: false,
      currentUser: "",
    }
  }

  componentDidMount(){
    FBSDK.AccessToken.getCurrentAccessToken().then(
      (data)=>{
         this._authenticationCompleted(data)
      }
    )
  }

  Logged(token)
  {
    console.log(token);
  }

  _authenticationCompleted(data)
  {
    if(data)
    {
      let credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken);

      firebase.auth().signInWithCredential(credential).then(
          (user)=> {
              let newUser = {
                  userID: user.uid,
                  displayName: user.displayName,
                  photoURL: user.photoURL
              }
              helpers.requestPermissions().then((granted)=> {
                if(granted)
                {
                  this.setState({login: true, authenticated: true, currentUser : newUser});
                }
              })
          }
      ).catch((error)=>{
        Alert.alert("Opps!", "Something went wrong :(", [
          {text: 'Retry', onPress: () => this._authenticationCompleted(data)},
        ])
      })
    }else{
      this.setState({login: false, authenticated: true});
    }
  }

  render() {
    return (
      this.auth()
    )
  }

  auth()
  {
    console.log("Changing State: " + this.state.authenticated);
    if (this.state.authenticated == false)
    {
      return (<View></View>);
    }
    else if (this.state.authenticated === true)
    {
      // return (<Root ref='RootNav' screenProps={{loggedIn: this.state.login, user: this.state.currentUser, setUser: this.setCurrentUser.bind(this)}}/>)
      return <NavWrapper screenProps={{loggedIn: this.state.login, user: this.state.currentUser, setUser: this.setCurrentUser.bind(this)}} 
                        initialRouteName={this.state.authenticated ? "Tabs": "Login"}/>
    }
  }

  setCurrentUser(user, callback)
  {
    console.log("Setting New User");
    this.setState({ login : true, currentUser: user, authenticated: true,},()=> {console.log("New user set, current user is:" + this.state.currentUser); callback();});
  }
}


