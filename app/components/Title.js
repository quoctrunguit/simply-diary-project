import React, {Component } from 'react';

import {
    Platform,
    StyleSheet,
    Text,
    View,
} from 'react-native';

import Recording_Test from './Recording_Test'

export default class Title extends React.PureComponent<{}>{

    render()
    {
        return(
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <View style={{flex: 1, justifyContent: 'flex-start'}}>
                    <Text style={styles.cardTitle}>At the moment</Text>
                </View>
                <View style={{flex: 1, flexDirection:'row', justifyContent: 'flex-end'}}>
                    
                </View>
            </View>
        )
    }

  
}

const styles= StyleSheet.create({
    cardTitle:{
        fontSize: 21,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 5 
    },
})

