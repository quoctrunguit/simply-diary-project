import React, {Component } from 'react';

import {
    Platform,
    PermissionsAndroid,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Switch
} from 'react-native';

import SoundRecorder from "react-native-sound-recorder"
import Sound from 'react-native-sound'
import Icon  from 'react-native-vector-icons/MaterialIcons'

const accent= "#119D8F"

const voiceIcon = (<Icon name="keyboard-voice" size={25} color={"gray"}/>)
const stopIcon = (<Icon name="stop" size={25} color="red"/>)

export default class Recording_Test extends React.PureComponent<{}>{

    constructor(props)
    {
        super(props)
        this.state= {
            switched: true,
        }
    }
  
    async startRecord()
    {
        await SoundRecorder.start(this.props.path)
        .then(function() {
          
            console.log('started recording');
        }).catch(
            function(error){
                console.log('There has been a problem with your fetch operation: ' + error.message);
                // ADD THIS THROW error
                 throw error;
            }
        );
    }

    async stopRecord()
    {
        await SoundRecorder.stop()
        .then(function(path) {
            console.log('stopped recording, audio file saved at: ' + path);
            return path;
        }).catch(
            function(error){
                console.log('There has been a problem with your fetch operation: ' + error.message);
                // ADD THIS THROW error
                 throw error;
            }
        );
    }

    async _play() {
        if (this.props.state.recording) {
            await this.stopRecord();
        }

        // These timeouts are a hacky workaround for some issues with react-native-sound.
        // See https://github.com/zmxv/react-native-sound/issues/89.
        setTimeout(() => {
            var sound = new Sound(this.props.path, '', (error) => {
            if (error) {
                console.log('failed to load the sound', error);
            }
            });

            setTimeout(() => {
                sound.play((success) => {
                    if (success) {
                    console.log('successfully finished playing');
                    } else {
                    console.log('playback failed due to audio decoding errors');
                    }
                });
            }, 100);
        }, 100);
    }

    render()
    {
        this.initialRecording();
        return(
           this.renderIcon()
        )
    }

    renderIcon()
    {
        let playIcon = (<Icon name="play-arrow" size={30}/>)
        let recordIcon = (<Icon name={this.props.state.recording ? "stop":  "fiber-manual-record"} size={21} color="red"/>);
        
        return (
            <View style={{flexDirection: 'row'}}>
                {this._displayControls()}
                {/* <View style={{flexDirection:'row', alignItems: 'center'}}>
                    {this.props.state.recording? <Text>Recording</Text>: null}
                    {this.props.state.recording ? <Text>{recordIcon}</Text> : null }
                </View> */}
                 
                {/* <TouchableOpacity onPress={()=> this.initialRecording()}>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=> this._play()}>
                    <Text>{playIcon}</Text>
                </TouchableOpacity> */}
            </View>
        )
    }

    _displayControls()
    {
        if(!this.props.state.recording)
        {
            return(
                <TouchableOpacity style={styles.recordButton} onPress={this._startRecord.bind(this)}>
                    <Text>{voiceIcon}</Text>
                </TouchableOpacity>);
        }else{
            return(
                <TouchableOpacity style={styles.stopButton} onPress={this._stopRecord.bind(this)}>
                    <Text>{stopIcon}</Text>
                    <Text style={{marginLeft: 5}}>recording</Text>
                </TouchableOpacity>);
        }
    }

    _startRecord()
    {

    }

    _stopRecord()
    {
        this.stopRecord().catch((error)=> {
            console.log(error);
        })
    }

    async initialRecording()
    {
        console.log("Recording State", this.props.state);
        if(this.props.state.recording)
        {
            this.startRecord().catch((error)=> {
                console.log(error);
            })
        }
        if(!this.props.state.recording)
        {
            return this.stopRecord().catch((error)=> {
                console.log(error);
            })
        }
    }
}

const styles=StyleSheet.create({
    stopButton:{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    recordButton:{
        justifyContent: 'center',
        alignItems: 'center',
       
    },
})


