import React, {Component } from 'react';

import {
    Platform,
    StyleSheet,
    Text,
    View,
} from 'react-native';

import MapView from 'react-native-maps'
import Icon from 'react-native-vector-icons/MaterialIcons'

const CIRCLERADIUS = 15
const RECT_WIDTH = 2
const RECT_HEIGHT = 30


const locIcon = (<Icon name="location-on" size={20} color="#119D8F" />)

export default class MapItem extends React.PureComponent<{}>{

    constructor(props)
    {
        super(props),
        this.state={
            textViewHeight: 0,
            loadMap: false
        }
    }

    _measureView(event) {
        this.setState({
            textViewHeight: event.nativeEvent.layout.height/2 -20
        })
    }

    render()
    {
        return(
            this._renderMap()
        )
    }

    _renderMap()
    {
        console.log("dataLogList:", this.props.data)
        var location =  this.props.data.length > 0 ? this.props.data[0].data.location : null;

        if(location == null)
        {
            console.log(location);
            return <View/>
        }else{
            var lat = location.latitude;
            var long = location.longitude;
            console.log("UUUUU", );
            return (    
                <View style ={styles.container}>
                    <View style={styles.timeLineSymbol}>
                        {/* render the circle */}
                        <View style={[{flex: 1}]}>
                        {/*render the cylinder*/}
                            <View style={[styles.rect, { opacity: this.props.index == 0 ? 0 : 1}]}/>  
                        </View>
                        <View style={{justifyContent: 'center'}}>
                           {locIcon}
                        </View>
                        <View style={{flex: 1}}>
                        {/*render the cylinder*/}
                            <View style={[styles.rect]}/>  
                        </View>
                    </View>
                    <View onLayout={(event)=>this._measureView(event)} style={styles.mapContainer}>
                        <MapView
                            style={styles.map}
                            region={{
                                latitude: lat,
                                longitude: long,
                                latitudeDelta: 0.00015,
                                longitudeDelta: 0.0000121,
                            }}>
                            <MapView.Marker
                                coordinate = {location} />
                        </MapView>
                    </View>
                </View>
            )
        }
    }
}

const styles = StyleSheet.create({
    circle:{
        width:CIRCLERADIUS,
        height:CIRCLERADIUS,
        borderRadius: CIRCLERADIUS/2,
        backgroundColor: '#119D8F',
    },
    rect: {
        position: 'absolute',
        top: 0,
        bottom: 5,
        width: RECT_WIDTH,
        backgroundColor: '#119D8F',
        marginTop: 5
    },
    timeLineSymbol:{
        flex: 0.1,
        flexDirection: 'column',      
        alignItems: 'center',
    },
    contentContainer:{
        flex: 1, 
        elevation: 2, 
        marginLeft: 20, 
        backgroundColor: 'white'
    },
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
      },
    mapContainer:{flex: 1,
        marginRight: 5,
        elevation: 2,
        backgroundColor: 'white'},
    map: {
        height: 150,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 5,
        marginBottom: 10,
    },
})
