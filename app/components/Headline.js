import React, {Component } from 'react';

import {
    Platform,
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    Image,
} from 'react-native';

import EventItemList from './EventItemList'
import DateTime from './DateTime'
import Collapsible from "react-native-collapsible"
import MapView from 'react-native-maps'
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialIcons';
import ProfileDisplay from './ProfileDisplay';

const accent ="#119D8F"
const cancelButton = (<Icon name="cancel" adjustsFontSizeToFit={true} size={21} color= "gray" />)
const saveButton = (<Icon name="class" adjustsFontSizeToFit={true} size={21} color= {accent} />)

export default class Headline extends Component<{}>{

    render()
    {
        var titleStyle= this.props.isClockHide ? styles.headlineTextJustify : styles.headlineTextLeft

        return(
            <View>
                {/*for the events and location*/}
                <Collapsible collapsed={this.props.isClockHide}>
                    <Image style={styles.headerImage}
                            source={{uri: 'https://firebasestorage.googleapis.com/v0/b/simplydiary-a32e0.appspot.com/o/header-photos%2Fheader-photo.jpg?alt=media&token=62f1ce8f-89ee-4e1a-a9a6-768914b1d744'}}/>
                    <View style={[styles.headerImage, {backgroundColor: 'black', opacity: 0.3}]}/>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <DateTime dateTimeInfo = {this.props.dateTimeInfo} currentTime={this.props.currentTime} showCalendar={this.props.showCalendar}/>
                        <ProfileDisplay UserInfo = {this.props.UserInfo}/>
                    </View>
                </Collapsible>
                
                <View style={{backgroundColor: 'white', elevation: 5}}>
                    <View style={{flexDirection: 'row', marginLeft: 15, marginRight: 5, marginTop: 10}}>
                        <View style={{flex: 0.7}}>
                            <TouchableOpacity onPress={()=> {this.props.focusChange(false)}}>
                                <Text style={titleStyle}>{this.props.isClockHide ? (this.props.headline == "" ? "No headline": this.props.headline) : "Headline"}</Text>
                            </TouchableOpacity>
                            <Collapsible collapsed={this.props.isClockHide}>
                                <TextInput ref='headline' editable={this.props.editable} 
                                            placeholder={"Name Your Day"} style={{}} 
                                            onChangeText={(value)=> this.props.onChangeText(value)} 
                                            value={this.props.headline}
                                            clearButtonMode="always"/>
                            </Collapsible>
                        </View>
                        <View  style={{ flex: 0.3, justifyContent:'center', alignItems:'center', flexDirection: 'row'}}>
                            
                            {this.props.isClockHide ? 
                            (<TouchableOpacity style={{alignItems: 'center', width: 50, height: 50}} onPress={()=> this.props.cancelButtonClicked()}>
                                <Text>{cancelButton}</Text>
                                <Text style={{fontSize: 13}}>cancel</Text>
                            </TouchableOpacity>) : null}

                            <TouchableOpacity style={{alignItems: 'center', width: 50, height: 50}} onPress = {this._logButtonClick.bind(this)}>
                                <Text>{saveButton}
                                </Text>
                                <Text style={{fontSize: 13}}>save</Text>
                            </TouchableOpacity>
                            {/* <Button
                                title={"Save"}
                                fontSize={13}
                                style={{alignItems: 'center', justifyContent: 'center'}}
                                large = {false}
                                //icon = {{name: 'send', iconStyle: {alignItems: 'center', justifyContent: 'center'}}}
                                buttonStyle={{backgroundColor: accent, borderRadius: 25, width: 100, height: 34}}
                                onPress = {this._logButtonClick.bind(this)}/> */}
                        </View>
                    </View>
                   
                </View>
            </View>
        )
    }

    _logButtonClick()
    {
        this.props.logButtonClick(this.props.diaryContent)
    }
}


const styles=StyleSheet.create({
    headlineTextLeft:{
        fontSize: 21
    },
    headlineTextJustify:{
        fontSize: 21,
        color: accent,
        fontWeight: 'bold'
    },
    mapContainer:{flex: 1,
        marginRight: 5,
        elevation: 2,
        backgroundColor: 'white'},
    map: {
        height: 60,
        marginLeft: 0,
        marginRight: 0,
        marginTop: 0,
        marginBottom: 5,
    },
    headerImage:{
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
    }
})


