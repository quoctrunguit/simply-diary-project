import React, {Component } from 'react';

import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity
} from 'react-native';
import MapItem from './MapItem';
import DiaryItem from './DiaryItem'



export default class DiaryLogItemList extends Component<{}>{

    constructor(props)
    {
        super(props);
        this.state={
            textViewHeight: 0,
        }
    }

  

    componentDidMount()
    {
        console.log(this.props);
    }

    render()
    {
        console.log("AAA", this.props.index)
        return(
            <View style={styles.itemListContainer}>
                <View style={styles.diaryLogContainer}>
                    {this._renderMap()}
                    {this._renderDiaryCompose()}
                </View>
            </View>
        )
    }

    _renderMap()
    {
        var loc = this.props.data[0].data.location
        if(loc.latitude == 0 && loc.longitude == 0)
        {
            return <View/>
        }
        return(
            <MapItem data = {this.props.data} index = {this.props.index}/>
        )
    }


    _renderDiaryCompose()
    {
        
        Date.daysBetween = function( date1, date2 ) {
            //Get 1 day in milliseconds
            var one_day=1000*60*60*24;
        
            // Convert both dates to milliseconds
            var date1_ms = date1.getTime();
            var date2_ms = date2.getTime();
        
            // Calculate the difference in milliseconds
            var difference_ms = date2_ms - date1_ms;
            
            // Convert back to days and return
            return Math.round(difference_ms/one_day*1000); 
        }

        var prop= this.props.data
        var _onPress = this.props.onPress;

        return this.props.data.map(
            function(element, index){

                var prevItem = index == 0 ? prop[0] : prop[index -1]
                var isLast = index == prop.length -1;

                var date = new Date(element.data.Date)
                var prevDate = new Date(prevItem.data.Date);

                var timeDiff = 0;
                if(index == 0)
                    timeDiff = 60
                else 
                {
                    timeDiff = Date.daysBetween(prevDate, date)
                }
                 
                
                return (
                   <DiaryItem key={index} data ={element} last ={isLast} timeDiff={timeDiff} index={index} onPress={_onPress}/>
                )
            }
        )
    }
}


const styles=  StyleSheet.create(
    {
        container: {
            flex: 1,
            backgroundColor: "white"
        },
        innerContainer:{
        },
        itemListContainer:{
            flex: 1,
            flexDirection: 'row',
            alignItems: "flex-start",
            marginTop: 5,
        },
        dateTimeContainer:{
            flexDirection: 'row',
        },
        timeContainer:{
            marginLeft: 5,
            flexDirection: 'column',
            justifyContent: 'flex-start',
        },
       
        diaryLogContainer:{
            flex: 1,
            flexDirection: 'column',
            alignItems: 'stretch'
        },
        dateText:{
            fontSize: 21,
            color: 'black'
        },

       
        
    }
)

