import React, {Component } from 'react';

import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity
} from 'react-native';

import Icon from "react-native-vector-icons/MaterialIcons"
import EventItemList from "./EventItemList"
import MapView from 'react-native-maps'
import * as Animatable from 'react-native-animatable';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';

const accent = "#119D8F"

const upArrow = (<Icon name="keyboard-arrow-up" size={20} color={accent}/>)

const selectedDay = {key:'selected', color: accent, selectedColor: 'blue'};

export default class PopUp extends React.PureComponent<{}>{

    constructor(props)
    {
        super(props);
        this.state = {
            selectedDay : null,
        }
    }

    render()
    {
        if(!this.props.displayDetails)
        {
            return null;
        }
        // var dateString = this.state.selectedDay.dateString;
        // var selectedDay = {dateString} : {selected: true, marked: true}
        return(
            <View style={styles.popUp}>
                <Animatable.View ref="container" animation="fadeInDown" duration={300} style={styles.animView}>
                    {/* <MapView
                        style={styles.map}
                        region={{
                            latitude: this.props.location.latitude,
                            longitude:  this.props.location.longitude,
                            latitudeDelta: 0.00015,
                            longitudeDelta: 0.0000121,
                        }}>
                        <MapView.Marker
                            coordinate = {this.props.location} />
                    </MapView> */}
                    <View style={styles.map}>
                        <Calendar 
                                markedDates = {{[this.state.selectedDay ? this.state.selectedDay.dateString : ""]:  {selected: true}}}
                             // Handler which gets executed on day press. Default = undefined
                                onDayPress={(day) => this._dayPressed(day)}
                                />
                    </View>
                    {this.props.events.length > 0 ? this.props.events.map(function(item){
                                    var event = {
                                        title: item.title ? item.title : "",
                                        location: item.location ? item.location: "",
                                        description: item.description ? item.description : ""
                                    }
                                    return <EventItemList data ={event}/>
                                }): <Text style={{marginLeft: 5, opacity: 0.5}}>No events today</Text>}
                    <TouchableOpacity onPress={()=>this._close()}>
                        <View style={styles.arrowContainer}>
                            {upArrow}
                        </View>
                    </TouchableOpacity>
                </Animatable.View>
           
            </View>
        )
    }

    _dayPressed(day)
    {
        this.setState({selectedDay: day});
        this.props.dayPress(day);
    }

    _close()
    {
        this.refs.container.fadeOutUp(300).then((state)=> this.props.onEventClick())
    }
}

const styles= StyleSheet.create({
    popUp:{
        position: 'absolute',
        left: 0,
        right: 0,
        top:0,
        bottom: 0,
        backgroundColor:'white',
        elevation: 6,
        opacity: 0.8
    },
    mapContainer:{flex: 1,
        marginRight: 5,
        elevation: 2,
        backgroundColor: 'white'},
    map: {
        height: 400,
        backgroundColor: 'white',
        marginLeft: 5,
        marginRight: 5,
        marginTop: 5,
        marginBottom: 10,
    },
    arrowContainer:{
        alignItems: 'center',
        justifyContent:'center',
    },
    animView:{
        backgroundColor: 'white',
        elevation: 1,
        opacity: 0
    }
})


