import React, {Component } from 'react';

import {
    Platform,
    StyleSheet,
    Text,
    View,
} from 'react-native';

export default class DiaryCover extends Component<{}>{
    render()
    {
        return(
            <View style={styles.cover}>
                <View style={styles.coverContainer}>
                    <View style={styles.titleContainer}>
                        <Text style={styles.diaryTitle}>DAY {this.props.dayth}</Text>
                        <Text style={styles.subTitle}>My diary</Text>
                    </View>
                </View>
            </View>
        )
    }
}

const styles= StyleSheet.create(
    {
        cover:{
            flex: 1,
            backgroundColor: 'gray',
            elevation: 2,
            marginLeft:5,
            marginRight: 5,
            marginTop: 10,
            borderRadius: 10
        },
        coverContainer:{
            flexDirection: 'column',
            alignItems: 'stretch',
            flex: 1,
        },
        titleContainer:{
            flexDirection: 'column',
            alignItems: 'flex-end',
            marginRight: 20,
            marginTop: 10
        },
        diaryTitle:{
            fontSize: 34,
            color: 'white',
        },
        subTitle:{
            fontSize: 21,
            color: 'white',
        }
    }
)



