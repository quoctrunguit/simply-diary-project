import React, {Component } from 'react';

import {
    Platform,
    StyleSheet,
    Text,
    View,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons'

const accentColor = '#119D8F'

const eventIcon = (<Icon name="event" color= {accentColor} />)
const desc = (<Icon name="description" color={accentColor}/>)
const loc = (<Icon name="location-on" color={accentColor}/>)

const CIRCLERADIUS = 5

export default class EventItemList extends React.PureComponent<{}>{
    render()
    {
        return(
            this._getInfo()
        )
    }

    _getInfo()
    {
        try{
            var title = this.props.data.title.replace(/ /g,'').length > 0 ?
            (
             <Text style={styles.eventTitle}>
             {this.props.data.title}
            </Text>) : null;
        }catch(error){
            console.log(error);
        }

        try{
            var location = this.props.data.location.replace(/ /g, '').length > 0 ? 
            (
                <Text style={styles.location}>
                    {loc} {this.props.data.location}
                </Text>
            ) : null;
        }catch(error){
            console.log(error);
        }

        try{
            var description = this.props.data.description.replace(/ /g, '').length > 0 ?
            (
                <Text style={styles.location}>
                    {desc} {this.props.data.description}
                </Text>
            ) : null;
        }catch(error){
            console.log(error);
        }

        return(
            <View style={styles.container}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <View style={styles.circle}/>
                    {title}
                </View>
                <View style={styles.eventSubtitle}>
                    {location}
                    {description}
                </View>
             
            </View>
        )
    }
}



const styles= StyleSheet.create({
    container:{
        marginTop: 5,
        marginLeft: 10,
        marginBottom: 10,
        flexDirection: 'column',
    },
    eventTitle:{
        color: '#119D8F',
        marginLeft: 10
    },
    eventSubtitle:{
        marginLeft: 20
    },
    circle:{
        width:CIRCLERADIUS,
        height:CIRCLERADIUS,
        backgroundColor: accentColor,
        borderRadius: CIRCLERADIUS/2,
        alignItems: 'center'
    },
})


