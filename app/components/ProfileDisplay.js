import React, {Component } from 'react';

import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
} from 'react-native';

const profileDimension=  50;

export default class ProfileDisplay extends React.PureComponent<{}>{
    render()
    {
        return(
            <View style={styles.container}>
                <Image style={styles.profilePhoto} source={{uri: this.props.UserInfo.photoURL ? this.props.UserInfo.photoURL :'' }}/>
                <Text style={styles.Name}>Hi! {this.props.UserInfo.displayName.split(' ')[0]}</Text> 
            </View>
        )
    }
}

const styles= StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    profilePhoto:{
        width: profileDimension,
        height: profileDimension,
        borderRadius: profileDimension/2,
    },
    Name:{
        fontWeight: 'bold',
        color: 'white'
    }
})
