import React, {Component } from 'react';

import {
    Platform,
    StyleSheet,
    Text,
    View,
    TextInput,
    ProgressBarAndroid,
    TouchableOpacity,
} from 'react-native';

import { Button } from 'react-native-elements';
import DiaryCover from '../components/DiaryCover'
import EventItemList from './EventItemList'
import Recording_Test from './Recording_Test'
import * as Animatable from 'react-native-animatable';
import Icon from "react-native-vector-icons/MaterialIcons"
import Collapsible from 'react-native-collapsible'

const accent ="#119D8F"

const backIcon = (<Icon name="arrow-downward" adjustsFontSizeToFit={true} size={21} color= {accent} />)
const restore = (<Icon name="restore" adjustsFontSizeToFit={true} size={21} color= "white" />)
const cancelButton = (<Icon name="cancel" adjustsFontSizeToFit={true} size={21} color= "gray" />)
//used for screen 'Home'

export default class DiarySheet extends Component<{}>{

    constructor(props)
    {
        super(props);
        this.state={
            diaryContent: "I am so happy"
        }

        console.log("DRAFT: ", this.props.draft);
    }

    render()
    {
        var loading = this.props.loading ? ( <View style={{position: 'absolute',elevation: 2, alignItems: 'center', justifyContent:'center', left: 0, right: 0, bottom: 0, top: 0}}>
                            <ProgressBarAndroid/> 
                        </View>) : null;
        return(
            <View style={[styles.container, {elevation: this.props.isClockHide ? 1 : 5}]}>
                <View style={styles.outerCard}>
                    {/*for the content*/}
                    <View style={styles.lowerCard}>

                        {/* Recording Control */}
                        <Recording_Test ref='recording' state = {this.props.state} 
                                        path = {this.props.path}
                                        permission = {this.props.permission}/>
                                        
                        {/* <View  style={{flex: 1, justifyContent:'flex-end', alignItems:'center', flexDirection: 'row'}}>
                            
                            {this.props.isClockHide ? 
                            (<TouchableOpacity onPress={()=> this.props.cancelButtonClicked()}>
                                <Text>{cancelButton}</Text>
                            </TouchableOpacity>) : null}
                            <Button
                                title={"Save"}
                                fontSize={13}
                                style={{alignItems: 'center', justifyContent: 'center'}}
                                large = {false}
                                //icon = {{name: 'send', iconStyle: {alignItems: 'center', justifyContent: 'center'}}}
                                buttonStyle={{backgroundColor: accent, borderRadius: 25, width: 100, height: 34}}
                                onPress = {this._logButtonClick.bind(this)}/>
                        </View> */}
                        {/* <Button
                            large = {false}
                            iconRight = {{name: 'photo'}}
                            buttonStyle={{backgroundColor: 'black', borderRadius: 5}}
                            textStyle={{textAlign: 'left'}}
                            title={`add photo`}
                            onPress = {()=> {
                                this.props.addPhoto()
                            }
                            }/>
                        <Button
                            title = {'get event'}
                            onPress = {()=> this.props.getEvent()}
                        /> */}
                    </View>
                    <View style={styles.innerCard}>
                        {loading}
                        <TextInput ref='diaryInput'  style={styles.diaryInput}
                                    placeholder={"How was your day ?"}
                                    editable={this.props.editable}
                                    multiline={true}
                                    textAlignVertical ='top'
                                    onChangeText={(input) => {
                                        this.props.typingStart();
                                        this.props.onChangeText(input);
                                    }}
                                    value={this.props.diaryContent}
                                    onFocus={()=>this.props.focusChange(true)}
                                    onEndEditing={()=>this.props.focusChange(false)}
                                    />
                    </View>
                </View>
                {/* <View style={styles.coverSection}>
                    <DiaryCover dayth = {this.props.dayth}/>
                </View> */}
               {this._renderDraftPop()}
            </View>
        )
    }

    _logButtonClick()
    {
        this.props.logButtonClick(this.props.diaryContent)
    }

    _renderDraftPop()
    {
       if(this.props.draft)
       {
           return (
            <Animatable.View ref='draftPopup'  style={styles.draftPopUp}>
               <TouchableOpacity  style={{flex: 1, flexDirection:'row'}} onPress={this._resumeDraftClicked.bind(this)}>
                    <View style={{ justifyContent: 'center', alignItems: 'flex-start', marginLeft: 20, marginRight: 20}}>
                        <Text adjustsFontSizeToFit style={{fontWeight:'bold', color:'white'}}>DRAFT</Text>
                    </View>
                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                        <View style={{flex: 0.7}}>
                            <Text numberOfLines={2} style={{color: 'white'}}>{this.props.draft.Content}</Text>
                        </View>
                        <View style={{flex: 0.3, justifyContent: 'center', alignItems: 'center'}}>
                            <Text>{this.props.draft ? restore : ""}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
              
            </Animatable.View>
           )
       }
       return null;
    }

    _renderBackButton()
    {
        if(!this.props.isClockHide)
            return null;

        return ( 
        <TouchableOpacity style={styles.returnButtonContainer} onPress={()=>this.props.focusChange(false)} >
            <Text>{backIcon}</Text>
        </TouchableOpacity>)
    }
    _resumeDraftClicked()
    {
        this.props.resumeDraft()
        if(this.refs.draftPopup)
        {
            this.refs.draftPopup.fadeOutDown(300).then((endstate)=> {this.refs.diaryInput.focus()})
        }
    }
}


const styles= StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white',
    },
    returnButtonContainer:
    {
        justifyContent: 'center', 
        alignItems:'center', 
        marginRight: 20
    },
    outerCard:{
        flex: 1,
        marginLeft: 10,
        marginRight: 10
    },
    innerCard:{
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
    },
    diaryInput:{
        alignSelf: 'stretch',
        flex: 1,
        backgroundColor: 'transparent',
        marginLeft: 5,
        marginRight: 5,
        marginTop: 5,
        
    },
    lowerCard:{
        flexDirection: 'row',
        marginTop: 5
    },
    coverSection:{
        position: 'absolute',
        flex: 0.382,
        height: 150,
        right: 0,
        left: 0,
        bottom: -10
    },
    draftButton: {
        justifyContent: 'center', 
        alignItems: 'center', 
        width: 100,
        marginRight: 10,
    },
    draftPopUp:{
        position:'absolute',
        flexDirection: 'row',
        bottom: 0,
        left: 0,
        right: 0,
        height: 50,
        backgroundColor: accent,
        elevation: 7
    }
});