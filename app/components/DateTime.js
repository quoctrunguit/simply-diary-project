import React, {Component } from 'react';

import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
} from 'react-native';


const textColor = 'white';


export default class DateTime extends React.PureComponent<{}>{
    componentDidUpdate()
    {
        // var date = new Date(this.props.dateTimeInfo.Date);
        // var month = this._getMonthName(date.getMonth);
    }

    render()
    {
        return(
            <TouchableOpacity style={styles.container} onPress={()=> this.props.showCalendar()}>
                <View style={[styles.smallerContainer]}>
                    <Text style={[styles.Day, styles.textContainer]}>{this.props.dateTimeInfo.Day}</Text>
                    <Text adjustsFontSizeToFit style={[styles.DayOfWeek]}>{this.props.dateTimeInfo.DayOfWeek}</Text>
                </View>
                <View style={[styles.biggerContainer]}>
                    <View style={{ flex: 1, marginLeft: 5, marginRight: 5, alignItems: 'center', justifyContent:'center'}}>
                        <Text adjustsFontSizeToFit style={[styles.Month]}>{this.props.dateTimeInfo.Month}</Text>
                        <Text style={[styles.Time]}>{this.props.currentTime}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    _getMonthName(month)
    {
        let months = {1: 'January', 2: 'February', 3: 'March', 4: 'April', 5: 'May', 6: 'June', 7: 'July', 8: 'August', 9: 'September',
                    10: 'October', 11: 'November', 12: 'December'}
        return months[month];
    }
}


const styles = StyleSheet.create({
    container:{
        flexDirection: 'row',
        alignItems: 'flex-start',
        marginRight: 5,
        marginTop: 5,
        marginLeft: 20,
        marginBottom: 5,
    },
    textContainer:{
        marginTop: 5
    },
    Time:{
        fontSize: 18,
        color: textColor,
    },
    Day:{
        fontSize: 34,
        color: textColor,
    },
    DayOfWeek:{
        fontSize: 18, 
        color: textColor,
    },
    Time:{
        fontSize: 21,
        color: textColor,
    },
    Month:{
        fontSize: 21,
        fontWeight: 'bold',
        color: textColor,
    },
    smallerContainer:{
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 5,
    },
    biggerContainer:{
        marginBottom: 5,
        marginLeft: 20,
        alignItems: 'center',
        justifyContent:'center',
    },
    rowContainer:{
        flex:1,
        flexDirection: 'row'
    }
});

