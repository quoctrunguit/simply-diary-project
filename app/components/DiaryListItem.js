import React, {Component} from 'react';

import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity
} from 'react-native';

import * as Helpers from '../helpers/helpers'
import Icon from "react-native-vector-icons/MaterialIcons"

const accent = "#119D8F";
const calendarIcon = (<Icon name="date-range" size={20} color={"gray"}/>)


export default class MyListItem extends React.PureComponent {


    render() {
        //const textColor = this.props.selected ? "red" : "black";
        return (
        <TouchableOpacity key={this.props.item.key} onPress={()=> this.props.OnPressItem(this.props.id)}
                         onLongPress={()=> this.props.OnLongPress(this.props.id)}>
           {this._renderListItem(this.props.item)}
        </TouchableOpacity>
        );
    }
    
    _onPress = () => {
        
    };

    _renderListItem(item)
    {
        //console.log(item);
        if(item == null)
        {
            console.error("Something wrong while retrieving diary logs");
            return (<View></View>)
        }

        let DateTime = Helpers.GetDateTimeObject(this.props.item.key)

        return(
            <View key = {this.props.item.key} style={styles.container}>
                <View style={styles.dateContainer}>
                    <View style={{flex: 1,  justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', elevation: 2, marginTop: 5, marginBottom: 5}}>
                        <Text style={styles.dateText}>{DateTime.Day}</Text>
                        <Text style={styles.dayOfWeek}>{DateTime.DayOfWeek}</Text>
                        <Text style={styles.monthYear}>{DateTime.Month} {DateTime.Year}</Text>
                    </View>
                </View>
                <View style={{flex: 0.618, }}>
                    <View style={{marginTop: 5, marginRight: 5, marginLeft: 10, marginBottom: 5}}>
                        <Text style={{color: 'gray', fontWeight: 'bold'}}>{item.data.latestLog ? (item.data.latestLog.headline ? item.data.latestLog.headline: ""): ""}</Text>
                        <Text numberOfLines={3} style={{marginTop: 10}}>{item.data.latestLog ? item.data.latestLog.content: ''}</Text>    
                    </View>
                </View>
            </View>
        )
    }
}

const styles= StyleSheet.create({
    container:{
        flex: 1,
        flexDirection: 'row',
        height: 100,
        backgroundColor: 'white',
        elevation: 1,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 5
    },
    dateContainer: {
        flex: 0.4,
        marginLeft: 5,
        flexDirection: 'row'
    },
    dateText:{
        fontWeight: 'bold',
        fontSize: 21,
        color: accent
    },
    dayOfWeek:{
        fontSize: 15,
    },
    monthYear:{
        opacity: 0.8
    }

})
