import React, {Component } from 'react';

import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

const CIRCLERADIUS = 10
const RECT_WIDTH = 2
const RECT_HEIGHT = 30

const itemMarginBottom = 10

const accent = "#119D8F"

const micIcon = (<Icon name="surround-sound" size={20} color={accent}/>)
const timeIcon = (<Icon name="av-timer" size={20} color={accent} />)

export default class DiaryItem extends React.PureComponent<{}>{
    //This part is to render the timeline columns and the diary logs

    constructor(props)
    {
        super(props),
        this.state={
            textViewHeight: 0,
            minorDiff: false,
            time: "",
        }
    }

    _measureView(event) {
        this.setState({
            textViewHeight: event.nativeEvent.layout.height
        })
    }

    componentWillMount()
    {
        var date = new Date(this.props.data.data.Date);
        var timeString = "";
        var isDiff = false;
        if (this.props.timeDiff >= 1440)
        {
            var unit = parseInt(this.props.timeDiff/1440) > 1 ? " days":" day";
            timeString = parseInt(this.props.timeDiff/1440) + unit + " later"
        }
        else if(this.props.timeDiff >= 60)
        {
            timeString = date.getHours() + ":"+date.getMinutes() +  (date.getHours()>=12 ? " PM":" AM")
        }else {
            timeString = this.props.timeDiff + " minutes later"
            isDiff = true;
        }
        this.setState({minorDiff: isDiff, time: timeString})
    }

    render()
    {
        //Render each logs in a card with respective timeline columns
        var size = this.state.minorDiff ? CIRCLERADIUS*0.5 : CIRCLERADIUS 
        var headline = this.props.data.data.headline ? <Text style={styles.headlineText}>{ this.props.data.data.headline}</Text > : null;
        var logContentAvailable = this.props.data.data.content.length > 0;
      
        return(
            <View style={styles.container}>
                <View style={[styles.timeLineSymbol,]}>
                   {/* render the circle */}
                    <View style={{flex: 0.1, justifyContent: 'flex-start'}}>
                        
                        {this.state.minorDiff ?
                            <View style={[styles.circle, {backgroundColor: this.state.minorDiff ? accent : 'transparent', width:size, height:size, borderRadius: size*0.5 }]}>
                            </View> : timeIcon}
                    </View>
                    <View style={{flex: 1, alignItems: 'center'}}>
                    {/*render the cylinder*/}
                            {this._renderRect()}
                    </View>
                </View>
               
                <View onLayout={(event)=>this._measureView(event)} style={[styles.contentContainer,{marginBottom: this.props.last ? 10:0}]}>
                    <TouchableOpacity onPress={()=> this.props.onPress(this.props.data.data)}>
                        <View>
                            <View  style={styles.timeContainer}>
                                <View style={{flex: 1, justifyContent:'flex-start'}}>
                                    <Text style={styles.timeText}>{this.state.time}</Text>
                                </View>
                                <View style={{flex: 1, flexDirection:'row', justifyContent: 'flex-end', marginRight: 20}}>
                                    {micIcon}
                                </View>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <View>
                        <View style={styles.textContainer}>
                            <View style={{flexDirection: 'row', justifyContent: 'flex-start', marginBottom: 5, marginLeft: 5,}}>
                                {headline}
                            </View>
                            <Text style={[styles.DiaryText, {opacity: logContentAvailable ? 1 : 0.3 }]}>{ logContentAvailable ? this.props.data.data.content :"..."}</Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
    _renderRect()
    {
        var color = '';
        var opacity = 1;
        if(this.props.last)
        {
            color= 'transparent';
        }else if(this.state.minorDiff){
            color = accent
            opacity = 0.7
        }else{
            color='#119D8F'
        }
        return(
            <View style={[styles.rect,{backgroundColor: color, opacity: opacity}]}/>  
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        flexDirection: 'row'
    },
    circle:{
        alignItems: 'center',
    },
    rect: {
        position: 'absolute',
        top: 0,
        bottom: 5,
        width: RECT_WIDTH,
        backgroundColor: accent
    },
    contentContainer:{
        flex: 1, 
        elevation: 2,
        marginRight: 5,
        backgroundColor: 'white',
    },
    textContainer:{
        flex: 1,
        marginBottom: itemMarginBottom,
        marginLeft: 15,
    },
    timeContainer:{
        flex: 0.3,
        marginLeft: 10,
        marginBottom: 10,
        marginTop: 5,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    timeLineSymbol:{
        flex: 0.1,
        flexDirection: 'column',      
        alignItems: 'center',
    },
    timeText:{
        fontSize: 13,
        fontWeight: 'bold',
        color: accent
    },
    DiaryText:{
        fontSize: 13,
        color: 'black',
        marginBottom: 5,
        marginLeft: 5,
        textAlign: 'justify'
    },
    headlineText:{
        fontWeight: 'bold',
        fontSize: 15,
    }
})


