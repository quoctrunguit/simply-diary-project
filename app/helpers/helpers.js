import {
    PermissionsAndroid,
    Platform,
} from 'react-native'


export function GetDateTimeObject(dateString)
{
    dateString = dateString.split('-').join('/');

    let d = new Date(dateString);

    if(!d)
    {
        console.error("Date Invalid");
    }

    var DateTime = {
        DateString: d.toLocaleDateString().split('/').join('-'),
        DayOfWeek: this._getDayOfWeek(d.getDay()),
        Month: this.getMonthName(d.getUTCMonth()+1),
        Day:  d.getDate(),
        Year: d.getFullYear(),
    }
    return DateTime
}

export function getMonthName(month)
{
    let months = {1: 'January', 2: 'February', 3: 'March', 4: 'April', 5: 'May', 6: 'June', 7: 'July', 8: 'August', 9: 'September',
                10: 'October', 11: 'November', 12: 'December'}
    return months[month];
}

export function _getDayOfWeek(dayth)
{
    let days = {1: 'Monday', 2: 'Tuesday', 3: 'Wednesday', 4: 'Thursday', 5: 'Friday', 6: 'Saturday', 7: 'Sunday'}
    return days[dayth];
}

export function requestPermissions()
{
    if (Platform.OS !== 'android') {
        return Promise.resolve(true);
    }

    //var requiredPermissions = PermissionsAndroid.PERMISSIONS.RECORD_AUDIO 
                                //PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION;

    // if(PermissionsAndroid.check(requiredPermissions)){
    //     return Promise.resolve(true);
    // }

   return PermissionsAndroid.requestMultiple([PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,PermissionsAndroid.PERMISSIONS.READ_CALENDAR]).then(results=>{
            console.log(results[PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION]);
            var success = results[PermissionsAndroid.PERMISSIONS.RECORD_AUDIO] === PermissionsAndroid.RESULTS.GRANTED &&
                results[PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION]=== PermissionsAndroid.RESULTS.GRANTED &&
                results[PermissionsAndroid.PERMISSIONS.READ_CALENDAR] === PermissionsAndroid.RESULTS.GRANTED;
            return Promise.resolve(success);
        });
}

export function checkPermission(permission)
{
    if (Platform.OS !== 'android') {
        return Promise.resolve(true);
    }

    return PermissionsAndroid.check(permission);
}
