import React, {Component } from 'react';

import {
    Platform,
    StyleSheet,
    Text,
    View,
    FlatList,
    ScrollView
} from 'react-native';

import firebase from 'react-native-firebase'
import Sound from 'react-native-sound'
import DiaryLogItemList from '../components/DiaryLogItemList'
import EventItemList from '../components/EventItemList'
import * as Helpers from "../helpers/helpers"

const database = null;
const storage = null;
const diaryLogs = "DiaryLogs"
const userLogs = "UserLogs"

const CIRCLERADIUS = 10
const RECT_WIDTH = 3
const RECT_HEIGHT = 30

const currentSound = null;

// Enable playback in silence mode
Sound.setCategory('Playback');

export default class DiaryDetail extends Component<{}>{

    constructor(props)
    {
        super(props);

        database= firebase.database().ref();
        storage = firebase.storage().ref();
        
        this.state = {
            diaryLogs: [],
            events: [],
            DateTimeInfo: Helpers.GetDateTimeObject(this.props.navigation.state.params.childKey),
            playing: false,
        }
    }

    componentDidMount(){

        //console.log(diaryLogs);
        let key = this.props.navigation.state.params.childKey;

        if(database)
        {
            this._retrieveLogs(key);
            let eventRef = database.child(userLogs).child(this.props.screenProps.user.userID).child(key).child("events");

            eventRef.once("value", (snapshot, _events)=>{
                _events =[]
                snapshot.forEach(function(childSnapshot){
                    let childData = childSnapshot.val();
                    let childKey = childSnapshot.key;
                    _events.push({key: childKey, data: childData})
                })
                this.setState({events: _events})
                console.log(_events);
            })
        }
    }


    _retrieveLogs(key)
    {
        let ref = database.child(diaryLogs).child(this.props.screenProps.user.userID).child(key);

       
        var temp = [];
        
        ref.once("value", (snapshot)=>this._intializeList(snapshot))
    }

    _intializeList(snapshot)
    {
        var dlogs = [];
        var newLogs = new Map();
        
        snapshot.forEach(function(childSnapshot){
            let childData = childSnapshot.val();
            let childKey = childSnapshot.key;
            dlogs.push({key: childKey, data: childData})
        })

        var results = [{key: this._toLocationString(dlogs[0].data.location), data: [dlogs[0]]}]

        for(var i = 1; i< dlogs.length; i++)
        {
            
            var element = dlogs[i];
            var _loc = this._toLocationString(element.data.location);
            var object = this._isExist(_loc, results);
            if(object != null)
            {
                object.data.push(element);
            }else{
                results.push({key: _loc, data: [element]});
            }
        }

        this.setState({diaryLogs : results}, console.log("Hello", this.state.diaryLogs));
    }

    _isExist(key, arr)
    {
        var result = null;
        arr.forEach(function(el){
            if(el.key == key)
            {
                result = el;
            }
                
        });
        return result;
    }

    _isSameLocation(a,b)
    {
       if(_toLocationString(a) == _toLocationString(b))
       {
           return true;
       }
       return false;
    }

    _toLocationString(_loc)
    {
         return Math.round(_loc.latitude).toString() +Math.round(_loc.longitude).toString();
    }
    
    render()
    {
        return(
            <View style={[styles.container]}>
                <View style={styles.headersContainer}>
                    <Text style={styles.dateText}>{this.state.DateTimeInfo.DayOfWeek}, {this.state.DateTimeInfo.Month} {this.state.DateTimeInfo.Day} {this.state.DateTimeInfo.Year}</Text>
                </View>
                <View style={styles.scrollViewContainer}>
                    <ScrollView style={styles.scrollView}>
                        <View style={styles.eventContainer}>
                            <View style={{flex: 0.1}}>
                            </View>
                            <View style={styles.innerEventContainer}>
                                <Text style= {styles.subHeader}>
                                    Events
                                </Text>
                                {this.state.events[0]? this.state.events.map(
                                    (value)=>{
                                        return(
                                            <EventItemList key = {value.key} data = {value.data}/>
                                        )
                                    }
                                ) : <Text style={styles.noEventText}>No recorded events!</Text>}
                            </View>
                           
                        </View>
                        <FlatList style={{flex: 1}}
                                data={this.state.diaryLogs}
                                renderItem={({item, index}) => this._renderItem(item, index)}
                                />
                    </ScrollView>
                </View> 
            </View>
        )
    }

    _renderItem(item, ind)
    {
        return (
            <DiaryLogItemList key ={item.key} index={ind} onPress={(data)=> this._onLogPress(data)} data = {item.data}/>
        )
    }

    _onLogPress(item)
    {
        if(this.state.playing)
        {
            if(currentSound)
                currentSound.stop(()=>this.setState({playing: false}));
        }else {
            this.setState({playing: true}, ()=> this._play(item.audioURL))
        }
    }

    async _play(path) {

        const callback = (error, sound) => {
            if (error) {
              sound.reset();
              Alert.alert('error', error.message);
              //setTestState(testInfo, component, 'fail');
              return;
            }
            //setTestState(testInfo, component, 'playing');
            // Run optional pre-play callbac
            // if(this.state.playing)
            // {
            //     sound.stop(()=>{

            //     })
            // }
            sound.play(() => {
              // Success counts as getting to the end
              //setTestState(testInfo, component, 'win');
              // Release when it's done so we're not using up resources
              sound.release();
            });
        };

        // These timeouts are a hacky workaround for some issues with react-native-sound.
        // See https://github.com/zmxv/react-native-sound/issues/89.
        //console.log(path);

        currentSound = new Sound(path, '', error => callback(error, currentSound));
        
        //var sound = new Sound(path, '', (error) => {
        // if (error) {
        //     console.log('failed to load the sound', error);
        // }
        // });
        // setTimeout(() => {
            
           

        //     setTimeout(() => {
        //         sound.play((success) => {
        //             if (success) {
        //             console.log('successfully finished playing');
        //             } else {
        //             console.log('playback failed due to audio decoding errors');
        //             }
        //         });
        //     }, 5000);
        // }, 5000);
    }
}

const styles=  StyleSheet.create(
    {
        container: {
            flex: 1,
            backgroundColor: 'white'
        },
        innerContainer:{
        },
        dateText:{
            fontSize: 25,
            color: 'black'
        },
        subHeader:{
            fontSize: 20,
            marginLeft: 5,
        },
        scrollView:{
            borderColor: 'red',
        },
        headersContainer:{
            marginLeft: 40,
            marginTop: 10,
        },
        scrollViewContainer:{
            flex: 1,
            borderColor: 'blue',
        },
        eventContainer:{
            flexDirection: 'row',
            marginBottom: 5,
            backgroundColor: 'white',
        },
        innerEventContainer:{
            flex: 1,
            marginBottom: 5,
            marginTop: 5,
            backgroundColor: "white"
        },
        noEventText: 
        {fontSize: 13, opacity: 0.8, marginLeft: 5, marginBottom: 10}
    }
)


