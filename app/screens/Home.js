import React, {Component } from 'react';

import {
    Platform,
    PermissionsAndroid,
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    Keyboard,
    Alert,
    BackHandler
} from 'react-native';

import DiarySheet from '../components/DiarySheet'
//import Recording from '../components/Recording'
import firebase from 'react-native-firebase'
import Title from '../components/Title'
import Headline from '../components/Headline'
import PopUp from '../components/PopUp'
import SoundRecorder from "react-native-sound-recorder"
import Collapsible from 'react-native-collapsible';
import * as Helpers from '../helpers/helpers'

//Calendar events
import RNCalendarEvents from 'react-native-calendar-events';


const database= null;
const storage = null;

var {height, width} = Dimensions.get('window');

var saveDraftID = null;

export default class Home extends Component<{}>{
    constructor(props)
    {
        super(props);
        this.state={
            DateTimeInfo: {
                DateString: "",
                DayOfWeek: "",
                Month: "",
                Day: "",
                Year: "",
            },
            currentTime: (new Date(Date.now())).toLocaleTimeString(),
            UserInfo: this.props.screenProps.user,
            Dayth: 5,
            audioPath: SoundRecorder.PATH_CACHE + '/audioCache.mp4',
            recordPermission: undefined,
            locationPermission: undefined,
            recorderState:{
                currentTime: 0.0,
                recording: false,
            },
            initialPosition:{
                longitude: 0,
                latitude: 0
            },
            headline:'',
            diaryContent: '',
            events: [],
            photoSource: undefined,
            displayDetails: false,
            loading: false,
            draftSaving: false,
            draft: "",
            hideClockView: false,
        }
        // BackHandler.addEventListener('hardwareBackPress', function() {
        //     // this.onMainScreen and this.goBack are just examples, you need to use your own implementation here
        //     // Typically you would use the navigator here to go to the last state.
        //     Alert.alert(
        //         'Exit',
        //         'Do you want to close the app ?',
        //         [
        //           {text: 'OK', onPress: this._closeApp},
        //           {text: 'Cancel', onPress: () => console.log("dissmised")},
        //         ],
        //         { cancelable: false }
        //       )
        // });
        // console.log("ConstructorL" + this.props.screenProps.user)
    }

    _closeApp()
    {
        BackHandler.exitApp();
    }

    watchID = null;

    componentWillMount(){

        //Start the clock
        setInterval(function(){
            this.setState({
                currentTime: (new Date(Date.now())).toLocaleTimeString()
            })
        }.bind(this), 1000);

        this.intializeDatabase();
        //check permissions
        Helpers.requestPermissions().then(granted => {
            if(granted)
            {
                this.setState({locationPermission: true, recordPermission: true}, ()=>{
                    var _today= (new Date(Date.now())).toLocaleDateString()
                    var _dateTime= Helpers.GetDateTimeObject(_today);
                    this.setState({DateTimeInfo: _dateTime}, ()=>{
                        this._retrieveDraft();
                    })
                    this._initializeCalenderEvents();
                    this._trackPosition();
                })
            }
        })
    }

    componentWillUnmount(){
        clearInterval();
    }

    intializeDatabase()
    {
        var success = false;
        if(firebase != null)
        {
            database = firebase.database().ref();
            storage = firebase.storage().ref();

            if(database && storage)
            {
                success = true;
            }
        }

        if(!success)
        {
           //something went wrongs
           Alert.alert("Opps", "Something went wrong, please check your network connection",[
                {text: 'Retry', onPress: () => this.intializeDatabase()},
            ])
        }

    }

    componentWillUnmount()
    {
        navigator.geolocation.clearWatch(this.watchID);
    }
    //Check for voice recording permission
    _checkRecordingPermission() {
        if (Platform.OS !== 'android') {
            return Promise.resolve(true);
        }

        if(PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.RECORD_AUDIO)){
           return Promise.resolve(true);
        }

        const rationale_mic = {
            'title': 'Microphone Permission',
            'message': 'Simply Diary needs to access your microphone to record your ambient sound'
        };


        return PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.RECORD_AUDIO, rationale_mic)
        .then((result) => {
            console.log("Microphone Acess: ", result)
            return (result === true || result === PermissionsAndroid.RESULTS.GRANTED);
        }).catch((error)=>console.warn(error));
    }

    _checkLocationPermission()
    {
        if (Platform.OS !== 'android') {
            return Promise.resolve(true);
        }

        if(PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)){
            return Promise.resolve(true);
         }

        const rationale_pos = {
            'title': 'Location Permission',
            'message': 'Simply Diary needs to access your microphone to record your daily position'
        };

        return PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, rationale_pos)
        .then((result)=>{
            console.log("Location Acess: ", result)
            return (result === true || result === PermissionsAndroid.RESULTS.GRANTED);
        });
    }

    _trackPosition()
    {
        console.log("Location Permission"+ this.state.locationPermission);
        console.log(this.state.recordPermission)
        //Track position on load
        if(this.state.locationPermission)
        {
            navigator.geolocation.getCurrentPosition((position)=>{
                var lat = parseFloat(position.coords.latitude);
                var long = parseFloat(position.coords.longitude);

                var pos = {
                    latitude: lat,
                    longitude: long
                }

                this.setState({initialPosition: pos})
            }, (error)=> console.log(error),
            {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}).

            watchID = navigator.geolocation.watchPosition((position)=>{
                var lat = parseFloat(position.coords.latitude);
                var long = parseFloat(position.coords.longitude);

                var pos = {
                    latitude: lat,
                    longitude: long
                }
                
                this.setState({initialPosition: pos})
            })
        }
    }

    render()
    {
        //set editable fields
        var _editable = !this.state.loading;
        return(
            <View style={styles.container}>
                {/* <Recording ref='recording'/> */}
                <View style={{}}>
                    <Headline  events= {this.state.events}
                                location ={this.state.initialPosition}
                                UserInfo = {this.state.UserInfo}
                                onChangeText={this._onHeadlineTextChange.bind(this)}
                                dateTimeInfo = {this.state.DateTimeInfo}
                                currentTime = {this.state.currentTime}
                                onEventClick={this.onEventClick.bind(this)}
                                editable = {_editable}
                                headline={this.state.headline}
                                isClockHide ={this.state.hideClockView}
                                showCalendar={this._showCalendar.bind(this)}
                                focusChange={this._focusChange.bind(this)}
                                logButtonClick={this._logButtonClicked.bind(this)}
                                cancelButtonClicked={this._cancel.bind(this)}
                                diaryContent={this.state.diaryContent}/>
                    {/* <Title  state = {this.state.recorderState} 
                            path = {this.state.audioPath}
                            permission = {this.state.hasPermission}/> */}
                </View>
              
                <DiarySheet userInfo = {this.state.UserInfo} 
                            dayth = {this.state.Dayth} 
                            diaryContent={this.state.diaryContent}
                            loading = {this.state.loading}
                            typingStart={this._typingStart.bind(this)}
                            addPhoto = {this._addPhotoClicked.bind(this)}
                            getEvent = {this._initializeCalenderEvents.bind(this)}
                            //Keyboard handlings
                            onChangeText={this._onChangeText.bind(this)}
                            focusChange={this._focusChange.bind(this)}
                            isClockHide ={this.state.hideClockView}
                            editable = {_editable}
                            //Draft functionalities
                            draftSaving={this.state.draftSaving}
                            draft = {this.state.draft}
                            resumeDraft={this._resumeDraft.bind(this)}
                            //For recording
                            state = {this.state.recorderState} 
                            path = {this.state.audioPath}
                            permission = {this.state.hasPermission}/>
                <Image style={{width: 100, height: 100}} source ={this.state.photoSource}/>
                <PopUp displayDetails = {this.state.displayDetails} events={this.state.events} 
                        location={this.state.initialPosition}
                        onEventClick={this.onEventClick.bind(this)}
                        dayPress={this._dayPressed.bind(this)}/>
            </View>
        )
    }

    _showCalendar()
    {
         //show pop up
         this.setState({displayDetails: !this.state.displayDetails});
    }

    _dayPressed(day)
    {
        console.log("Day selected",day);
        this.setState({DateTimeInfo:  Helpers.GetDateTimeObject(day.dateString)})
    }

    _focusChange(value)
    {
        this.setState({hideClockView: value});
        if(!value)
        {
            Keyboard.dismiss();
        }
    }

    _resumeDraft()
    {
        this.setState({diaryContent: this.state.draft.Content, headline: this.state.draft.Headline});
    }


    _saveDraft()
    {
        saveDraftID = setInterval(function(param){
            var data = {Headline: this.state.headline, Content: this.state.diaryContent}
            this.setState({draftSaving: true})
            let _userInfo = this.state.UserInfo;
            if(database && storage)
            {
                console.log("Start Saving Draft", data)
                database.child("UserLogs")
                .child(_userInfo.userID)
                .child(this.state.DateTimeInfo.DateString)
                .child("draft").child("0").set(data).then((result) => {
                    this.setState({draftSaving: false})
                }).catch((error)=>{
                    console.error(error);
                    this.setState({draftSaving: false})
                });
            }
        }.bind(this), 3000)
    }

    _retrieveDraft()
    {
        try{
            console.log("Retrieving Draft")
            //Get draft
            if(database)
            {
                var _userInfo = this.state.UserInfo;
                
                let date = this.state.DateTimeInfo.DateString;

                database.child("UserLogs")
                .child(_userInfo.userID)
                .child(date)
                .child("draft").once("value", (snapshot)=>{
                    var _draft = undefined;
                    snapshot.forEach(function(childSnapshot){
                        var childKey = childSnapshot.key;
                        var childData = childSnapshot.val();
                        _draft=  childData;
                        console.log(_draft);
                        if(_draft.Content.replace(/ /g, '').length === 0)
                            _draft = undefined;
                    })
                    console.log("Draft retrieved for: " + date + "is: " + _draft);
                    this.setState({draft: _draft});
                })
            }
        }catch(error)
        {
            console.log("Error during retrieving draft: " + error);
        }
        
    }

    onEventClick()
    {
        //show pop up
        this.setState({displayDetails: !this.state.displayDetails});
    }

    _onChangeText(content)
    {
        this.setState({diaryContent: content});
    }

    _onHeadlineTextChange(value)
    {
        this.setState({headline: value});
    }

    async _typingStart()
    {
        if(this.state.recorderState.recording == false)
        {
            let _recordState= {
                currentTime: 0.0,
                recording: true,
            }
            //if is not recording, start recording
            console.log("Debug: recording started")
            this._saveDraft({Headline: this.state.headline, Content: this.state.diaryContent});
            this.setState({recorderState : _recordState})
        }
    }

    _logButtonClicked(diaryContent)
    {
        console.log("Debug: Sending to server with a hope of working");
        console.log("Debug: State:", this.state);

        console.warn(diaryContent, this.state.headline);
        if(!diaryContent) diaryContent = "";

        if(diaryContent.replace(/ /g,'').length == 0)
        {
            Alert.alert(
                'Blank Page 😞',
                'Please write something about your day',
                [
                  {text: 'OK', onPress: () => this._cancel()},
                ],
                { cancelable: false }
              )
            return;
        }

        var data = {Headline: this.state.headline ? this.state.headline : "", Content: diaryContent}
        // Stop the recording
        if(this.state.recorderState.recording)
        {
            let _recordState= {
                currentTime: 0.0,
                recording: false,
            }
            //stop recording, start loading
            this.setState({recorderState: _recordState, loading: true});
        }

        //upload to the database
        this._uploadToDatabase(data);

        //Stop saving draft:
        clearInterval(saveDraftID);
    }

    _addPhotoClicked()
    {
        var ImagePicker = require('react-native-image-picker');

        var options = {
            title: 'Select Photo',
            customButtons: [
                {name: 'fb', title: 'Choose Photo from Facebook'},
            ],
            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        };
        
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
          
            if (response.didCancel) {
              console.log('User cancelled image picker');
            }
            else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
              console.log('User tapped custom button: ', response.customButton);
            }
            else {
              let source = { uri: response.uri };
          
              // You can also display the image using data:
              // let source = { uri: 'data:image/jpeg;base64,' + response.data };
          
              this.setState({
                photoSource: source
              });
            }
          });
          
    }
    
    _initializeCalenderEvents()
    {
        var from = new Date(Date.now())
        from = from.toLocaleDateString();
        from = new Date(from).toISOString();
        var to = new Date();
        to.setDate(to.getDate() + 1)
        to = to.toLocaleDateString();
        to = new Date(to).toISOString();

        console.log(from, to);
        this._getCalendarEvents(from, to);

        //this.setState({events: this._getCalendarEvents(from, to)}, console.log("Debug: events loaded", this.state.events.length));
    }

    _getCalendarEvents(from, to)
    {
        var _events = []

        RNCalendarEvents.authorizeEventStore().then(
            result => {
                console.log(result)
                if(result == 'authorized')
                {
                    console.log(from, to);
                    RNCalendarEvents.fetchAllEvents(from, to)
                    .then(events => {
                        events.forEach(
                            event => {
                                var e = {
                                    title : event.title,
                                    description: event.description,
                                    location: event.location
                                }
                                _events.push(e);
                            }
                        )
                    })
                    .catch(error => {
                        console.error(errors)
                    });
                    this.setState({events: _events}, console.log("CalendarLoaded"));
                }else{
                    console.error("Calendar Event unauthorized");
                }
            }
        ).catch(error=> console.warn(error));
        return _events;
    }

    _getCallLogs()
    {
        CallLogs.show((logs) =>{
            // parse logs into json format
             const parsedLogs = JSON.parse(logs);

             console.log(parsedLogs);
             
            // logs data format
            /*
              [
                { 
                  phoneNumber: '9889789797', 
                  callType: 'OUTGOING | INCOMING | MISSED',
                  callDate: timestamp,
                  callDuration: 'duration of call in sec',
                  callDayTime: Date()
                },
                .......
               ]
            */
           });
    }

    _uploadToDatabase(data)
    {
        try{
            if(database && storage)
            {
                //First, upload to storage
                 // require the module
    
                console.log("Start Writing to database");
    
                var metadata = {
                    contentType: 'video/mp4',
                };
    
                storage.child(this.state.UserInfo.userID).child(this.state.currentTime + '.mp4').put(this.state.audioPath, metadata).then((result)=>{
                    console.log(result);
                    let _userInfo = this.state.UserInfo;
                    let perDayPayload = {
                        Date: new Date(Date.now()),
                        DateTimeInfo: this.state.DateTimeInfo,
                        headline: data.Headline,
                        content: data.Content,
                        imageURL: '',
                        audioURL: result.downloadURL,
                        location:{
                            latitude: this.state.initialPosition.latitude,
                            longitude: this.state.initialPosition.longitude
                        },
                    }
    
                    let ref = database.child("DiaryLogs").child(_userInfo.userID).child(this.state.DateTimeInfo.DateString)
                       .push(perDayPayload);
    
                    database.child("UserLogs")
                        .child(_userInfo.userID)
                        .child(this.state.DateTimeInfo.DateString)
                        .child("Logs")
                        .push(ref.path);
    
                    database.child("UserLogs")
                        .child(_userInfo.userID)
                        .child(this.state.DateTimeInfo.DateString)
                        .child("latestLog").set(perDayPayload);
    
                    database.child("UserLogs")
                    .child(_userInfo.userID)
                    .child(this.state.DateTimeInfo.DateString)
                    .child("events").set(this.state.events);
    
                    this.setState({diaryContent: '', headline: '', loading: false, hideClockView: false})
                }).catch((error)=>{
                    console.log("error uploading file: ", error);
                })
            }
        }catch(error){
            console.log("Error while connecting to the database: "+ error);
        }
       
    }

    _cancel()
    {
        // Stop the recording
        let _recordState= {
            currentTime: 0.0,
            recording: false,
        }
        //stop recording, start loading
        this.setState({recorderState: _recordState, loading: true, diaryContent: '', headline: '', loading: false,  hideClockView: false}, ()=> Keyboard.dismiss());
    }
}


const styles = StyleSheet.create({
    container:{
        flex: 1,
    },
   
});

