import React, {Component } from 'react';

import {
    Platform,
    StyleSheet,
    Text,
    View,
    SectionList,
    FlatList,
    TouchableOpacity,
    Alert
} from 'react-native';

import firebase from 'react-native-firebase'
import DateTime from '../components/DateTime';
import DiaryListItem from '../components/DiaryListItem'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';

const database = null;
const storage = null;
const diaryLogs = "DiaryLogs"
const userLogs = "UserLogs"

const accent= "#119D8F"

const calendarIcon = (<Icon name="today" color={accent}/>)

export default class DiaryList extends Component<{}>{

    constructor(props)
    {
        super(props);
        database= firebase.database().ref();
        storage = firebase.storage().ref();

        this.state = {
            dataSource: this._getDiaryLogs(),
        }
    }


    _getDiaryLogs()
    {   
        let ref = database.child(userLogs).child(this.props.screenProps.user.userID);
    

        let index = 0;
        let diaryLogs = [];

        //get diary logs by day
        ref.once('value', (snapshot)=>{
            snapshot.forEach(function(childSnapshot){
                var childKey = childSnapshot.key;
                var childData = childSnapshot.val();
                diaryLogs.push({key: childKey, data: childData});
            })
        })

       // this.setState({dataSource: diaryLogs.slice(0)},console.log("Debug: k - D:", diaryLogs));
       return diaryLogs;
       
    }

    componentWillMount()
    {
        var user = this.props.screenProps.user;
        console.log("Getting database reference from: " + user.userID )
        let ref = database.child("UserLogs").child(user.userID);

        ref.on('value', (snapshot, diaryLogs)=> {
            diaryLogs = [];
            snapshot.forEach(function(childSnapshot){
                var childKey = childSnapshot.key;
                var childData = childSnapshot.val();
                diaryLogs.push({key: childKey, data: childData});
            })
            diaryLogs.sort(function(a,b){
                aString = a.key.split('-').join('/');
                bString = b.key.split('-').join('/');
                let aD = new Date(aString);
                let bD = new Date(bString);
                return bD-aD;
            });
            this.setState({dataSource: diaryLogs}, console.log("Diary List Retrieved: " + this.state.dataSource));
        })
    }

    render()
    {
        return(
            <View style={styles.container}>
                <View style={{flex: 1}}>
                    <FlatList
                        data={this.state.dataSource}
                        renderItem={({item}) => this._renderListItem(item)}
                    />
                </View>
            </View>
        )
    }

    _renderListItem = (i)=>{
        return (<DiaryListItem
                    key = {i.key}
                    id={i.key}
                    OnPressItem = {this._onPressItem.bind(this)}
                    OnLongPress = {this._onLongPress.bind(this)}
                    item = {i}
                />)
    }

    _renderYearFilters()
    {
        var yearFilters = new Map();

        var Button = (year, index)=> (
        <TouchableOpacity key={index}>
            <View style={styles.filterButton}>
                <Text style={styles.filterText} adjustsFontSizeToFit={true}>{year}</Text>
            </View>
        </TouchableOpacity>)

        return(
            this.state.dataSource.map(function(item, index){
                var date = new Date(item.data.latestLog ? item.data.latestLog.Date : null);
                var year = date.getFullYear()

                if(!date) return;

                if( yearFilters.has(year) == false){
                    yearFilters.set(year, date);
                    return Button(year, index)
                }
            })
        )
        console.log("DATA SOURCE", this.state.dataSource);
    }
    
    _keyExtractor = (item, index) => item.id;

    _onPressItem(key)
    {   
        this.props.navigation.navigate('DiaryDetail', {childKey: key});
    }

    _onLongPress(key)
    {
        //var newDataSource= this.state.dataSource.filter((item)=> item.key != key);
        
        Alert.alert("Actions", "Do you want to delete the entry?",[
            {text: "OK", onPress: ()=> {this._deleteAnEntry(key)}}
        ])

        //this.setState({dataSource: newDataSource})
    }

    _deleteAnEntry(key)
    {
        console.log(this.state);
        var user = this.props.screenProps.user;
        console.log("Getting database reference from: " + user.userID )
        console.log(database);
        let ref = database.child("UserLogs").child(user.userID);
        let diaryRef = database.child("DiaryLogs").child(user.userID)
        
        ref.child(key).remove().then((result)=> diaryRef.child(key).remove());
    }
    
    
    _getDayOfWeek(dayth)
    {
        let days = {1: 'Monday', 2: 'Tuesday', 3: 'Wednesday', 4: 'Thursday', 5: 'Friday', 6: 'Saturday', 7: 'Sunday'}
        return days[dayth];
    }

}

const styles= StyleSheet.create({
    container:{
        flex: 1,
        marginTop: 5
    },
    filterButton:{
        backgroundColor: accent,
        borderRadius: 10,
    },
    filterText:{
        marginLeft: 5,
        marginRight: 5,
    }
});


