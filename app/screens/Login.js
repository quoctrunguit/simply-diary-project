import React, {Component } from 'react';

import {
    Platform,
    StyleSheet,
    Text,
    View,
} from 'react-native';

import Icon from "react-native-vector-icons/Entypo"
import firebase from 'react-native-firebase'


const FBSDK = require('react-native-fbsdk');

const {
  LoginButton,
  AccessToken
} = FBSDK;

const accent = "#119D8F"

const featherIcon = (<Icon name="feather" size={50} color={accent}/>)

export default class Login extends Component<{}>{


    constructor(props)
    {
        super(props);
    }

    componentWillMount(){
        // console.log(FBSDK.AccessToken);
        // FBSDK.AccessToken.getCurrentAccessToken().then(
        //   (data)=>{
        //     if (data == null) //token unavailable
        //         this.refreshCurrentAccessTokenAsync().then(
        //             ()=> this.Logged()
        //         )
        //     else 
        //         this._authenticationCompleted(data.accessToken)
        //   }
        // )

        // console.log(firebase.storage().ref())
        if(this.props.screenProps.loggedIn)
        {
            this.props.navigation.navigate('Home');
        }
    }

    render()
    {
        return(
            <View style={styles.container}>
                <View style={{flexDirection: 'column'}}>
                    <View style={{flexDirection: 'row', alignItems: 'flex-end'}}>
                        <View>
                            <Text style={styles.title}>Hi</Text>
                            <Text style={styles.subTitle}>I'm <Text style={styles.Brand}>Simply Diary</Text></Text>
                        </View>
                        <View>
                            <Text>{featherIcon}</Text>
                        </View>
                    </View>
                    
                    <View style={{marginTop: 20}}> 
                        <Text style={{marginBottom: 10}}>Let's sign-in!</Text>
                        <LoginButton
                            publishPermissions={["publish_actions"]}
                            onLoginFinished={
                                (error, result) => {
                                if (error) {
                                    alert("login has error: " + result.error);
                                } else if (result.isCancelled) {
                                    alert("login is cancelled.");
                                } else {
                                    AccessToken.getCurrentAccessToken().then(
                                    (data) => {
                                        this._finalizeAuthentication(data.accessToken)
                                    }
                                    )
                                }
                            }
                        }
                        onLogoutFinished={() => alert("logout.")}/>
                    </View>
                </View>
               
               
            </View>
        )
    }

    _finalizeAuthentication(token)
    {
        let credential = firebase.auth.FacebookAuthProvider.credential(token);

        var signedIn = firebase.auth().signInWithCredential(credential).then(
            (user)=> {
                let newUser = {
                    userID: user.uid,
                    displayName: user.displayName,
                    photoURL: user.photoURL,
                }
                console.log("New user: ", newUser);
                //WIP - Add user to the database
                database = firebase.database().ref();
                this.props.screenProps.setUser(newUser, this.navigateHome.bind(this));
            }
        ).catch((error)=> console.warn("Something went wrong:"+error));
       //signedIn.then().catch((error)=> console.log("Navigation Error:" + error));
    }

    navigateHome()
    {
        console.log("Navigating To Home");
        this.props.navigation.navigate("Home")
    }
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
    title:{
        fontSize: 30,
    },
    subTitle:{
        fontSize: 21,
        fontWeight: 'bold',
    },
    Brand:{
        color: "#119D8F"
    }
  });
  