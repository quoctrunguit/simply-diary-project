import React from 'react';
import { TabNavigator, StackNavigator } from 'react-navigation';
import {
  Platform,
  StyleSheet,
  Text,
  View,
} from 'react-native';

import Login from '../screens/Login';
import Settings from '../screens/Settings'
import Home from '../screens/Home';
import DiaryList from '../screens/DiaryList'
import DiaryDetail from '../screens/DiaryDetail'
import Icon from 'react-native-vector-icons/Entypo'

const accent = "#119D8F"

const featherIcon = (<Text><Icon name="feather" size={20} color={"white"}/></Text>)

export const MyDiaryStack = StackNavigator({
  DiaryList: {
    screen: DiaryList,
    navigationOptions: {
      title: 'Feed',
    },
  },
  DiaryDetail: {
    screen: DiaryDetail,
    navigationOptions: ({ navigation }) => ({
      title: "Detail",
    }),
  },
},
{
  mode: 'modal',
  headerMode: 'none',
});

export const Tabs = TabNavigator({
  Home: {
    screen: Home,
    navigationOptions:({navigation}) => ({
      tabBarLabel: featherIcon,
      tabBarIcon: ({ tintColor }) => <Icon name="list" size={35} color={tintColor} />,
    }),
  },
  MyDiary: {
    screen: MyDiaryStack,
    navigationOptions: {
      tabBarLabel: 'My Diary',
      tabBarIcon: ({ tintColor }) => <Icon name="account-circle" size={35} color={tintColor}/>
    },
  },
},{
  tabBarOptions:{
    style:{
      backgroundColor: accent
    }
  },
  initialRouteName: "Home",
});

export const SettingsStack = StackNavigator({
  Settings: {
    screen: Settings,
    navigationOptions: {
      title: 'Settings',
    },
  },
});

const Navigator = ({ initialRouteName, screenProps }) => {
  const stackNavigatorConfigs = {
    initialRouteName,
    navigationOptions: ({ navigation }) => ({
      headerStyle: styles.headerStyle,
      headerLeft: <Left navigation={navigation} />,
    }),
  };
  console.log("Changing props: "+ screenProps);

  const CustomNavigator = screenProps.loggedIn === false ? StackNavigator ({
      Login:{
        screen: Login,
      },
    }):  StackNavigator({
    Tabs: {
      screen: Tabs,
    },
    Settings: {
      screen: SettingsStack,
    },
  }, {
    mode: 'modal',
    headerMode: 'none',
    initialRouteName,
  });
  return <CustomNavigator screenProps={screenProps} />;
};

export const NavWrapper = ({ initialRouteName, screenProps }) => (
  <Navigator screenProps={screenProps} initialRouteName={initialRouteName} />
);


export const Root = StackNavigator({
  Login:{
    screen: Login,
  },
  Tabs: {
    screen: Tabs,
  },
  Settings: {
    screen: SettingsStack,
  },
}, {
  mode: 'modal',
  headerMode: 'none',
});


